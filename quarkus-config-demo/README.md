# quarkus-config-demo Project

This project contains a pair of examples that configure a Quarkus application.  They're based on Antonio Goncalves' 
chapter in "Understanding Quarkus" which is a book that I recommend.

## String-Based Configuration

The first example is StringsInvoicesResource.  This service returns an Application Scoped Invoice object with several
fields annotated with @ConfigProperty.  These property values are set with values stored in src/resources/application.xml.

For instance, the field "terms" in me.carlwalker.strings.Invoice will receive the value "Payment upon receipt" which
is stored under the app > invoice > terms key.  The key is set as an attribute of the annotation.

## Typesafe Configuration

The second example is TypesafeInvoicesResource.  This uses an additional class @InvoiceConfiguration which will hold
the configuration values.  It is Type Safe which means that the program gathers a set of values as a unit rather than
relying on a string convention.  It has the downside that you will need to manage the assignment of these values
to the Invoice object.

### Profiles

TypesafeInvoicesResource is also outfitted with Profiles.  This is the Quarkus' technique for managing different
environment configurations.  For instance, there are values marked with "(d)" showing that a configuration was pulled
from a dev configuration and a "(t)" showing that a configuration was from a test configuration.  To apply a Profile,
run with 

````
-Dquarkus.profile=test
````

### .env File

TypesafeInvoicesResource can be additionally configured by dropping a .env file into the working directory.  This file
*must* be called .env (and not local.env).

Because Profiles are being used, the following prefix is required to override a property

```
_DEV_INV_TERMS=terms from env (d)
```
