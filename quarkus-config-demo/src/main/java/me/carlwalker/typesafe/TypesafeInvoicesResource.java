package me.carlwalker.typesafe;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/typesafe/invoices")
public class TypesafeInvoicesResource {

    @Inject
    Invoice invoice;

    @Inject
    InvoiceConfiguration invoiceConfiguration;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInvoices() {

        invoice.setVatAmount( invoiceConfiguration.vatRate() );
        invoice.setAllowsDiscount( invoiceConfiguration.allowsDiscount() );
        invoice.setTerms(invoiceConfiguration.terms());
        invoice.setPenalties(invoiceConfiguration.penalties());
        invoice.setDiscountRate(invoiceConfiguration.discountRate());

        return Response
                .ok()
                .entity( List.of(invoice) )
                .build();
    }
}
