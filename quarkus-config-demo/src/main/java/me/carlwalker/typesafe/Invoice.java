package me.carlwalker.typesafe;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.enterprise.context.RequestScoped;
import java.util.Objects;

@RegisterForReflection
@RequestScoped
public class Invoice {

    @JsonProperty
    private Float subtotal;

    @JsonProperty
    private Float vatRate;

    @JsonProperty
    private Float vatAmount;

    @JsonProperty
    private Float total;

    @JsonProperty
    private Boolean allowsDiscount;

    @JsonProperty
    private Float discountRate;

    @JsonProperty
    private String terms;

    @JsonProperty
    private String penalties;

    public Float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Float subtotal) {
        this.subtotal = subtotal;
    }

    public Float getVatRate() {
        return vatRate;
    }

    public void setVatRate(Float vatRate) {
        this.vatRate = vatRate;
    }

    public Float getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(Float vatAmount) {
        this.vatAmount = vatAmount;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Boolean getAllowsDiscount() {
        return allowsDiscount;
    }

    public void setAllowsDiscount(Boolean allowsDiscount) {
        this.allowsDiscount = allowsDiscount;
    }

    public Float getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Float discountRate) {
        this.discountRate = discountRate;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPenalties() {
        return penalties;
    }

    public void setPenalties(String penalties) {
        this.penalties = penalties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        me.carlwalker.typesafe.Invoice invoice = (me.carlwalker.typesafe.Invoice) o;
        return Objects.equals(subtotal, invoice.subtotal) && Objects.equals(vatRate, invoice.vatRate) && Objects.equals(vatAmount, invoice.vatAmount) && Objects.equals(total, invoice.total) && Objects.equals(allowsDiscount, invoice.allowsDiscount) && Objects.equals(discountRate, invoice.discountRate) && Objects.equals(terms, invoice.terms) && Objects.equals(penalties, invoice.penalties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subtotal, vatRate, vatAmount, total, allowsDiscount, discountRate, terms, penalties);
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "subtotal=" + subtotal +
                ", vatRate=" + vatRate +
                ", vatAmount=" + vatAmount +
                ", total=" + total +
                ", allowsDiscount=" + allowsDiscount +
                ", discountRate=" + discountRate +
                ", terms='" + terms + '\'' +
                ", penalties='" + penalties + '\'' +
                '}';
    }
}