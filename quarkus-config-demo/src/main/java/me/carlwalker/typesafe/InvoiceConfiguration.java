package me.carlwalker.typesafe;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix="inv",namingStrategy = ConfigMapping.NamingStrategy.VERBATIM)
public interface InvoiceConfiguration {
    Float vatRate();
    Boolean allowsDiscount();
    Float discountRate();
    String terms();
    String penalties();
}
