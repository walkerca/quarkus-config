package me.carlwalker.strings;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/strings/invoices")
public class StringsInvoicesResource {

    @Inject
    Invoice emptyInvoice;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInvoices() {
        return Response
                .ok()
                .entity( List.of(emptyInvoice) )
                .build();
    }
}
